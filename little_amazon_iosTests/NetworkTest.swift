//
//  NetworkTest.swift
//  little_amazon_iosTests
//
//  Created by Massimiliano Ceriani on 13/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import XCTest

class NetworkTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    //--------------------------------------------------------------------
    func test_login_user(){
        
        let loginEx = expectation(description: "Login")
        
        NetworkManager.get_shared().login(email: "email@email.com", password: "passowrd",
                                          on_success: { user in
             loginEx.fulfill()
            
        }, on_generic_error: {
             loginEx.fulfill()
            XCTFail()
            
        }, on_access_forbidden: {
             loginEx.fulfill()
            XCTFail()
            
        }, on_parameter_error: {
             loginEx.fulfill()
            XCTFail()
        })
        
        
        waitForExpectations(timeout: 10, handler: nil)
    }
    //--------------------------------------------------------------------
    func test_get_get_products(){
        
        let loginEx = expectation(description: "product")
        
        NetworkManager.get_shared().get_all_products(on_success: { (products) in
            
            
            XCTAssertTrue(products.count >= 1)
            
            loginEx.fulfill()
            
        }, on_generic_error: {
            
            XCTFail()
        }, on_network_error: {
            
            XCTFail()
        })
        
        waitForExpectations(timeout: 10, handler: nil)
        
    }

    
    public func test_insert_product(){
        
        let loginEx = expectation(description: "product")
        
        NetworkManager.get_shared().create_new_product(product: Product(id: 1, name: "prova", itemsNumber: 78, price: 78), on_success: {
            
            
            XCTAssertTrue(true)
            
            loginEx.fulfill()
            
        }, on_generic_error: {
              XCTFail()
        }) {
            XCTFail()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
        
    }
    
    public func test_create_order_for_user(){
        
        
        let loginEx = expectation(description: "createorder")
        
        NetworkManager.get_shared().create_new_order_for_user(
            order: Order(id: "davideOrderid",
                         idUser: "davideUserid",
                         date: "data",
                         products: [Product(id: 1, name: "max", itemsNumber: 89, price: 78)]),
            
            on_success: {
            
            XCTAssertTrue(true)
            
            loginEx.fulfill()
        }, on_error: {
             XCTFail()
        }, on_network_error: {
            
            XCTFail()
        })
        
        waitForExpectations(timeout: 10, handler: nil)
        
    }
    
}
