//
//  NetworkManager.swift
//  little_amazon_ios
//
//  Created by Massimiliano Ceriani on 13/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NetworkManager{
    
    //Shared instance
    private static var shared_instance: NetworkManager?
    
    public static func get_shared()->NetworkManager{
        
        if shared_instance == nil{
            //initialize
            shared_instance = NetworkManager()
        }
        
        return shared_instance!
    }
    
    //URL
    private let gateway_basic_url: String = "192.168.99.100"
    private let gateway_endpoint: Int = 8080
    private let gateway_protocol: String = "http"
    
    private var gateway_final_url: String{
        get{
            return "\(gateway_protocol)://\(gateway_basic_url):\(gateway_endpoint)"
        }
    }
    
    //enum with all the paths
    private enum BasicPath: String{
        case user = "user"
        case product = "product"
        case order = "order"
        case iot = "iot"
        
    }//end BasicPath
    
    private enum SupplementaryPath: String{
        case user_login = "login"
        case product_quantity = "quantity"
        case order_user = "user"
        case iot_associaton = "association"
        case iot_associaton_user = "association/user"
    }
    
    //============================================================
    //          USER - LOGIN
    //============================================================
    public func login(email: String, password: String, on_success: @escaping (User)->(),
                      on_generic_error: @escaping ()->(),
                      on_access_forbidden: @escaping ()->(),
                      on_parameter_error: @escaping()->()){
        
        //0. setup parameters
        let param: Parameters = [
            "email" : email,
            "md5_password": password
        ]

        //1. get url
        let finalURL = "\(self.gateway_final_url)/\(BasicPath.user.rawValue)/\(SupplementaryPath.user_login.rawValue)"
        
        
        
        //2. perform request
        Alamofire.request(finalURL, method: .post, parameters: param, encoding: JSONEncoding.default).responseString { (response) in
            
            /*
             200:
             description: User with that email and pasword exist
             schema:
             $ref: '#/definitions/user_service_User'
             400:
             description: generic error
             401:
             description: access forbidden
             422:
             description: Parameter error
             */
            
            if let status_code = response.response?.statusCode, let _ = response.result.value  {
                do{
                    print(response.value!)
                    
                    
                    let json = try JSON(data: response.value!.data(using: String.Encoding.utf8)!)
     
                    switch status_code{
                    case 200:
                        
                        
                        on_success(User(id: json["_id"].stringValue,
                                        rev: json["_rev"].stringValue,
                                        name: json["name"].stringValue,
                                        surname: json["surname"].stringValue,
                                        email: json["email"].stringValue,
                                        password: json["password_md5"].stringValue,
                                        date: json["date"].intValue))
                    case 422:
                        on_parameter_error()
                    case 401:
                        on_access_forbidden()
                    default:
                        on_generic_error()
                    
                    }
                }catch{
                    on_generic_error()
                }
            }else{
                on_generic_error()
            }
        }//end request
    }
    //============================================================
    //          USER - GET ALL USER
    //============================================================
    public func get_all_user( success: @escaping ()->()){
        

        //1. get url
        let finalURL = "\(self.gateway_final_url)/\(BasicPath.user.rawValue)"
        
        //2. perform request
        Alamofire.request(finalURL, method: .get, parameters: nil, encoding: JSONEncoding.default).responseString { (response) in
            
            
            print("\n")
            print(String(describing: response.result.value))
            success()
        }
    }//end login
    
    //============================================================
    //          PRODUCT
    //============================================================
    public func get_all_products(on_success: @escaping ([Product])->(),
                                 on_generic_error: @escaping ()->(),
                                 on_network_error: @escaping ()->()){
        
        
        
        //1. get url
        let finalURL = "\(self.gateway_final_url)/\(BasicPath.product.rawValue)"
        
        //2. perform request
        Alamofire.request(finalURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseString { (response) in
            
            
            if let _ = response.result.value , let status = response.response?.statusCode{
                
                
                switch status{
                case 200:
                    
                    do{
                        let json = try  JSON(data: response.value!.data(using: String.Encoding.utf8)!)
                        
                        //to return
                        var allProducts: [Product] = []
                        
                        //extract json array
                        let product = json["rows"].arrayValue
                        
                        for i in product{
                         
                            //append new product
                            let toAdd = Product(
                                id: i["doc"]["_id"].stringValue,
                                name: i["doc"]["name"].stringValue,
                                itemsNumber: i["doc"]["quantity"].intValue,
                                price: i["doc"]["price"].intValue)
                            
                            toAdd.urlImage = i["doc"]["urlImage"].string
                            
                            allProducts.append(toAdd)
                        }
                        
                        on_success(allProducts)
                        
                        
                        
                    }catch{
                        on_generic_error()
                    }
                    
                    
                    
                default:
                    on_generic_error()
                }
                
            }else{
                on_network_error()
            }
        }
        
        
    }
    
    
    public func create_new_product(product: Product, on_success: @escaping ()->(),
                                 on_generic_error: @escaping ()->(),
                                 on_network_error: @escaping ()->()){
        
        
        
        //1. get url
        let finalURL = "\(self.gateway_final_url)/\(BasicPath.product.rawValue)"
        
        //
        /*
         {
         "name": "prodotto1",
         "price": 178,
         "quantity":12
         }
         */
        let param: [String: Any] = ["name": product.name,
                                       "price": product.price,
                                       "quantity": product.itemsNumber]
        
        //2. perform request
        Alamofire.request(finalURL, method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil).responseString { (response) in
            
            
            if let _ = response.result.value , let status = response.response?.statusCode{
                
                
                switch status{
                case 200:
                    
                   on_success()
                    
                    
                default:
                    on_generic_error()
                }
                
            }else{
                on_network_error()
            }
        }
        
        
    }
    
    //============================================================
    //          ORDER
    //============================================================
    public func create_new_order_for_user(
        order: Order,
        on_success: @escaping ()->(),
        on_error: @escaping ()->(),
        on_network_error: @escaping ()->()){
        
        
        //1. get url
        let finalURL = "\(self.gateway_final_url)/\(BasicPath.order.rawValue)"
        
        //
        /*
         {
         "name": "prodotto1",
         "price": 178,
         "quantity":12
         }
         */
        
        var product_param: [[String: Any]] = []
        
        if let x = order.products{
            
            for i in x{
                
                var x : [String: Any] = [
                    "idProduct" : i.id,
                    "name" : i.name,
                    "price" : i.price,
                    "quantity" : i.userSelectionNumber
                ]
                
                //add url image if exists
                if let urlImg = i.urlImage {
                    x.updateValue(urlImg, forKey: "urlImage")
                }
                
                
                product_param.append(x)
            }
            
        }
      
        let param: [String: Any] = ["userID": order.idUser,//order.idUser,
                                    "item_list": product_param]
        
        print(param)
        
        //2. perform request
        Alamofire.request(
            finalURL,
            method: .post,
            parameters: param,
            encoding: JSONEncoding.default,
            headers: nil).responseString { (response) in
            
            
            if let _ = response.result.value , let status = response.response?.statusCode{
                
                
                switch status{
                case 200:
                    
                    on_success()
                    
                    
                default:
                    
                    on_error()
                }
                
            }else{
                on_network_error()
            }
        }
        
        
    }
    
    public func get_order_for_user(
        user: User,
        on_success: @escaping ([Order])->(),
        on_error: @escaping ()->(),
        on_network_error: @escaping ()->()){
        
        
        //1. get url
        let finalURL = "\(self.gateway_final_url)/\(BasicPath.order.rawValue)/user/" + user.id
        
        
        //2. perform request
        Alamofire.request(finalURL, method: .get).responseString { (response) in
            
            
            if let _ = response.result.value , let status = response.response?.statusCode{
                
                
                switch status{
                case 200:
                    
                    do{
                        let json = try  JSON(data: response.value!.data(using: String.Encoding.utf8)!)
                        
                        //to return
                        var allOrders: [Order] = []
                        
                        //extract json array
                        let orders = json["docs"].arrayValue
                        
                        for i in orders{
                            
                            var allProducts: [Product] = []
                            
                            //parse products
                            for p in i["item_list"].arrayValue {
                                let prod = Product(
                                    id: p["_id"].stringValue,
                                    name: p["name"].stringValue,
                                    itemsNumber: p["quantity"].intValue,
                                    price: p["price"].intValue)
                                
                                prod.urlImage = p["urlImage"].string
                                
                                allProducts.append(prod)
                            }
                            
                            //append new product
                            let toAdd = Order(
                                id: i["_id"].stringValue,
                                idUser: i["userID"].stringValue,
                                date: "",
                                products: allProducts
                            )
                            
                            allOrders.append(toAdd)
                        }
                        
                        on_success(allOrders)
                        
                        
                        
                    }catch{
                        on_error()
                    }
                    
                    
                default:
                    
                    on_error()
                }
                
            }else{
                on_network_error()
            }
        }
        
        
    }
    
    public enum AmountType: String {
        case EUR = "EUR"
        case USD = "USD"
    }
    
    public func getAmount(for order: Order, type: AmountType, success: @escaping (Int) -> (), errorHandler: @escaping ()->()) {
        //1. get url
        let finalURL = "\(self.gateway_final_url)/\(BasicPath.order.rawValue)/" + order.id + "/amount/" + type.rawValue
        
        Alamofire.request(finalURL, method: .get).responseJSON { (response) in
            
            switch(response.result) {
            case .success(let value):
                let amount = JSON(value)["amount"].intValue
                success(amount)
                
            case .failure(let error):
                print(error.localizedDescription)
                errorHandler()
            }
        }
        
    }
    
    
    
    
    
}//end class
