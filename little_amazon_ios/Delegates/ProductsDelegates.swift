//
//  ProductsDelegates.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 19/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import Foundation
import UIKit

extension ProductsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ProductCell else {
            return UICollectionViewCell()
        }
        
        let product = viewModel.productForRow(index: indexPath.row)
        cell.setupCell(product: product)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 10.0, bottom: 0, right: 10.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentProduct = viewModel.productForRow(index: indexPath.row)
        
        if currentProduct.itemsNumber > 0 {
            
            //check if element already exists
            if let product = CurrentUsage.sharedInstance.productToOrder[currentProduct.id] {
                //if product already exists -> simply decrememnt product
                product.decrementProduct()
            } else {
                //if product doesn't exists, first decrement it and then add it to dict
                currentProduct.decrementProduct()
                CurrentUsage.sharedInstance.productToOrder[currentProduct.id] = currentProduct
            }
            
            //perform animation from cell to tabitem
            performAnimation(fromIndex: indexPath.row)
            
            //update this cell to update quantity
            collectionView.reloadItems(at: [indexPath])
        }
    }
    
    
    
}
