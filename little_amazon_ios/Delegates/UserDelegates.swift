//
//  UserDelegates.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 20/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberRowForTable()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? ProfileCell else {
            return UITableViewCell()
        }
        
        let tuple = viewModel.getInfoCell(at: indexPath.row)
        cell.setupCell(key: tuple.0, value: tuple.1)
        
        return cell
    }
}
