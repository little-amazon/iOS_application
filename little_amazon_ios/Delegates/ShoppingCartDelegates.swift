//
//  ShoppingCartDelegates.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 20/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

extension ShoppingCartViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //retrive cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? ShoppingCartCell else {
            return UITableViewCell()
        }
        
        //retrive product from viewmodel and assign it to cell
        let product = viewModel.getProductForIndex(index: indexPath.row)
        cell.setupCell(product: product)
        
        //cell UI
        cell.backgroundColor = .clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rowNum = viewModel.getTableRowNumber()
        
        //create empty label
        if rowNum == 0 {
            let lbl = UILabel(frame: tableView.bounds)
            lbl.text = "No Items"
            lbl.font = UIFont(name: "OpenSans", size: 20.0)
            lbl.textColor = UIColor.darkGray
            lbl.textAlignment = .center
            tableView.backgroundView = lbl
        } else {
            tableView.backgroundView = nil //remove label if not empty
        }
        
        return viewModel.getTableRowNumber()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
}
