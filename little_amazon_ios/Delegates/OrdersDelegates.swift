//
//  OrdersDelegates.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 24/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit
import Kingfisher

extension OrderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rowNum = viewModel.getRowNumber()
        
        //create empty label
        if rowNum == 0 {
            let lbl = UILabel(frame: tableView.bounds)
            lbl.text = "Nothing ordered yet"
            lbl.font = UIFont(name: "OpenSans", size: 20.0)
            lbl.textColor = UIColor.darkGray
            lbl.textAlignment = .center
            tableView.backgroundView = lbl
        } else {
            tableView.backgroundView = nil //remove label if not empty
        }
        
        return rowNum
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? OrderCell else {
            return UITableViewCell()
        }
        
        cell.backgroundColor = .clear //clear background
        cell.setupCell(idx: indexPath.row, order: viewModel.getOrderAtIndex(index: indexPath.row), currency: currentCurrency)
        
        return cell
    }
}

extension OrderCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentOrder.products?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collCell", for: indexPath)
        guard let imageV = cell.viewWithTag(10) as? UIImageView else {
            return UICollectionViewCell()
        }
        if let products = currentOrder.products, let imgStrUrl = products[indexPath.row].urlImage {
            imageV.kf.setImage(with: URL(string: imgStrUrl))
        }
        
        return cell
    }
}
