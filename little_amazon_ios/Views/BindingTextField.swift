//
//  BindingTextField.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 14/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class BindingTextField: UITextField {

    var textChanged :(String) -> () = { _ in }
    
    func bind(callback :@escaping (String) -> ()) {
        
        self.textChanged = callback
        self.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField :UITextField) {
        self.textChanged(textField.text!)
    }

}
