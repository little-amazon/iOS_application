//
//  ProductCell.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 20/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit
import Kingfisher

class ProductCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productItemsNumberLabel: UILabel!
    @IBOutlet weak var addCartButton: UIButton!
    
    func setupCell(product: Product) {
        productPriceLabel.text = "\(product.price ?? 0),00 €"
        productNameLabel.text = product.name
        productItemsNumberLabel.text = "\(product.itemsNumber ?? 0) left"
        
        //color label red if products empty
        if product.itemsNumber == 0 {
            productItemsNumberLabel.textColor = .red
        } else {
            productItemsNumberLabel.textColor = .black
        }
        
        //set image if exists
        if let urlString = product.urlImage {
            productImageView.kf.setImage(with: URL(string: urlString))
        } else {
            productImageView.image = UIImage(named: "item_placeholder.png")
        }
        
        
        //disable button -> need to allow to tap all cell
        addCartButton.isUserInteractionEnabled = false
    }
    
}
