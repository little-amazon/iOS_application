//
//  FLButton.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 19/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class FLLoginButton: UIButton {

    private var gradientLayer: CAGradientLayer?

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        //set gradient
        gradientLayer = CAGradientLayer()
        gradientLayer!.frame = rect
        gradientLayer!.colors = [#colorLiteral(red: 0.7843137255, green: 0.4392156863, blue: 0.3725490196, alpha: 1), #colorLiteral(red: 0.7137254902, green: 0.2941176471, blue: 0.4196078431, alpha: 1)].map({$0.cgColor})
        gradientLayer!.startPoint = CGPoint.init(x: 0, y: 0)
        gradientLayer!.endPoint = CGPoint.init(x: 1, y: 1)
        layer.insertSublayer(gradientLayer!, at: 0)
    }
}
