//
//  FLFloatingButton.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 20/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class FLFloatingButton: UIButton {
    
    private var gradientLayer: CAGradientLayer?

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        //set gradient
        gradientLayer = CAGradientLayer()
        gradientLayer!.frame = rect
        gradientLayer!.cornerRadius = 12.0
        gradientLayer!.colors = [#colorLiteral(red: 0.7843137255, green: 0.4392156863, blue: 0.3725490196, alpha: 1), #colorLiteral(red: 0.7137254902, green: 0.2941176471, blue: 0.4196078431, alpha: 1)].map({$0.cgColor})
        gradientLayer!.startPoint = CGPoint.init(x: 0, y: 0)
        gradientLayer!.endPoint = CGPoint.init(x: 1, y: 1)
        layer.insertSublayer(gradientLayer!, at: 0)
        
        //corner radius
        layer.cornerRadius = 12.0
        
        //shadow
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize.init(width: 1, height: 1)
        layer.shadowRadius = 5.0
        layer.shadowOpacity = 0.98
        layer.shadowPath = UIBezierPath.init(roundedRect: frame, cornerRadius: 12.0).cgPath
    }

}
