//
//  OrderCell.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 24/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var productsNumberLabel: UILabel!
    @IBOutlet weak var productsListLabel: UILabel!
    @IBOutlet weak var productsCollection: UICollectionView!
    @IBOutlet weak var amountLabel: UILabel!
    
    internal var currentIndex: Int = 0
    internal var currentOrder: Order!
    

    public func setupCell(idx: Int, order: Order, currency: NetworkManager.AmountType) {
        currentIndex = idx
        currentOrder = order
        orderNumberLabel.text = "Order #\(idx + 1)"
        
        if let products = order.products {
            productsNumberLabel.text = "\(products.count) products ordered"
            
            //clear label before asssign
            productsListLabel.text = ""
            for p in products {
                let strToAdd = "- " + p.name + " x" + String(p.itemsNumber)
                if p === products.last {
                    productsListLabel.text = productsListLabel.text! + strToAdd
                } else {
                    productsListLabel.text = productsListLabel.text! + strToAdd + "\n"
                }
                
            }
        }
        
        //currency label
        NetworkManager.get_shared().getAmount(for: order, type: currency, success: { (amount) in
            if currency == .EUR {
                self.amountLabel.text = "\(amount) €"
            } else {
                self.amountLabel.text = "\(amount) $"
            }
        }, errorHandler: {})
        
        //assign delegate
        productsCollection.delegate = self
        productsCollection.dataSource = self
    }
}
