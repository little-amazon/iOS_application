//
//  ShoppingCartCell.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 20/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit
import Kingfisher

class ShoppingCartCell: UITableViewCell {

    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productQuantityLabel: UILabel!
    @IBOutlet weak var productAmountLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    
    public func setupCell(product: Product) {
        productTitleLabel.text = product.name
        productQuantityLabel.text = "\(product.userSelectionNumber) choosen"
        
        let amount = product.price * product.userSelectionNumber
        productAmountLabel.text = "\(amount),00 €"
        
        //assign image
        if let urlString = product.urlImage {
            productImageView.kf.setImage(with: URL(string: urlString))
        }
    }

}
