//
//  FLLoader.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 20/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class FLLoader: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var loaderImageView: UIImageView!
    
    private static var currentLoader: UIViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("FLLoader", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [
            .flexibleWidth,
            .flexibleHeight
        ]
        
        //set gif to imageview
        let gif = UIImage(gifName: "flamingo.gif")
        loaderImageView.setGifImage(gif)
    }
    
    public class func show(in controller: UIViewController) {
        
        let v = FLLoader(frame: UIScreen.main.bounds)
        v.alpha = 0.9
        v.loaderImageView.startAnimatingGif()
        
        let modalVC = UIViewController()
        modalVC.modalPresentationStyle = .custom
        modalVC.modalTransitionStyle = .coverVertical
        modalVC.view = v
        
        //asssign current loader
        currentLoader = modalVC
        
        controller.present(modalVC, animated: true, completion: nil)
    }
    
    public class func hide() {
        if let currentLoader = currentLoader {
            currentLoader.dismiss(animated: true, completion: nil)
        }
    }

}
