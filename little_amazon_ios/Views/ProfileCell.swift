//
//  ProfileCell.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 20/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var attrTitleLabel: UILabel!
    @IBOutlet weak var attrDetailLabel: UILabel!
    
    func setupCell(key: String, value: String) {
        attrTitleLabel.text = key.uppercased()
        attrDetailLabel.text = value
    }

}
