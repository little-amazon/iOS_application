//
//  ShoppingCartViewController.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 20/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class ShoppingCartViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var amount: FLFloatingButton!
    
    internal var viewModel: ShoppingCartViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9058823529, blue: 0.9137254902, alpha: 1)
        
        //crete view model
        viewModel = ShoppingCartViewModel()
        
        //table UI
        tableView.tableFooterView = UIView()
        tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 80.0, right: 0)
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 30, bottom: 0, right: 0)
        tableView.separatorColor = tableView.separatorColor?.withAlphaComponent(0.5)
        tableView.backgroundColor = .clear

        //assign delegates
        tableView.delegate = self
        tableView.dataSource = self
        
        //setup title
        self.navigationItem.title = "Carrello"
        self.navigationController?.navigationBar.titleTextAttributes = [
            .font : UIFont.init(name: "OpenSans", size: 15.0)!
        ]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //reload every display
        tableView.reloadData()
        
        //amount button title upadte
        updateAmountButton()
    }
    
    private func updateAmountButton() {
        //amount button
        let amountButtonTitle = "Confirm Order \(viewModel.retriveButtonTotalAmount()),00 €".uppercased()
        amount.setTitle(amountButtonTitle, for: .normal)
    }
    
    
    @IBAction func amountButtonAction(_ sender: Any) {
        if CurrentUsage.sharedInstance.productToOrder.count <= 0 {
            return
        }
        
        let alertController = UIAlertController(title: "Confirm order", message: "Are you sure?", preferredStyle: .alert)
        let confirmAction: (UIAlertAction) -> () = { action in
            //check for user
            guard let userID = CurrentUsage.sharedInstance.currentUser?.id else {return}
            
            //get products
            let allProducts = Array(CurrentUsage.sharedInstance.productToOrder.values)
            
            //create order
            let o = Order(id: "", idUser: userID, date: "", products: allProducts)
            
            //show loader
            FLLoader.show(in: self)
            
            //upload orders
            NetworkManager.get_shared().create_new_order_for_user(order: o, on_success: {
                
                FLLoader.hide()
                
                //empty shopping cart
                CurrentUsage.sharedInstance.emptyProduct()
                
                //reload table
                self.tableView.reloadData()
                
                //amount button title upadte
                self.updateAmountButton()
                
                //update bottom tabbar badge
                if let tabItems = self.tabBarController?.tabBar.items, tabItems.count > 1 {
                    tabItems[1].badgeValue = nil
                }
            
            }, on_error: {
                FLLoader.hide()
            
            }, on_network_error: {
                FLLoader.hide()
            })
        }
        
        //add actions
        alertController.addAction(UIAlertAction(title: "Confirm", style: .default, handler: confirmAction))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        //show alert
        present(alertController, animated: true, completion: nil)
    }
}
