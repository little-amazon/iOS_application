//
//  OrderViewController.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 24/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class OrderViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    internal var viewModel: OrderViewModel!
    internal var currentCurrency: NetworkManager.AmountType = .EUR

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9058823529, blue: 0.9137254902, alpha: 1)
        
        //setup navigation title
        self.navigationItem.title = "I miei ordini"
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font : UIFont.init(name: "OpenSans", size: 15.0)!
        ]
        
        //view model
        viewModel = OrderViewModel()
        
        //table ui
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        
        //delegates
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //get orders reloading table
        viewModel.retriveOrders(success: {
            self.tableView.reloadData()
        }) {
            //show errors
        }
        
        
    }
    
    
    @IBAction func segmentedAction(_ sender: UISegmentedControl) {
        currentCurrency = sender.selectedSegmentIndex == 0 ? .EUR : .USD
        tableView.reloadData()
    }
    
}
