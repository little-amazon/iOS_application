//
//  ProfileViewController.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 20/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var userImageV: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    internal var viewModel: ProfileViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup top navigation - clear color
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()

        //view model
        viewModel = ProfileViewModel()
        
        //setup image UI
        userImageV.layer.borderColor = UIColor.white.cgColor
        userImageV.layer.borderWidth = 1.0
        userImageV.layer.cornerRadius = userImageV.bounds.width / 2
        userImageV.clipsToBounds = true
        
        //table ui
        tableView.tableFooterView = UIView()
        tableView.separatorColor = tableView.separatorColor?.withAlphaComponent(0.3)
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 50, bottom: 0, right: 0)
        
        //table delegates
        tableView.delegate = self
        tableView.dataSource = self
    }

    @IBAction func logoutButtonAction(_ sender: Any) {
        //close current session
        CurrentUsage.sharedInstance.closeSession()
        
        //logout
        dismiss(animated: true, completion: nil)
    }
}
