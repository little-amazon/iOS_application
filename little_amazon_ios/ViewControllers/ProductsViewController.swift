//
//  ProductsViewController.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 19/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    internal var viewModel: ProductViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9058823529, blue: 0.9137254902, alpha: 1)
        
        viewModel = ProductViewModel()

        //assign delegates
        collectionView.delegate = self
        collectionView.dataSource = self
        
        //setup navigation title
        self.navigationItem.title = "Tutti i prodotti"
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font : UIFont.init(name: "OpenSans", size: 15.0)!
        ]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //declare handlers
        let successHandler = { [unowned self] in
            self.collectionView.reloadData()
        }
        
        //declare error handler
        let errorHandler: ProductViewModel.ProductRetriveError = { [unowned self] errorType in
            switch errorType {
            case .Generic:
                break
            case .Network:
                break
            }
        }
        
        //perform call
        viewModel.retriveData(successHandler: successHandler, errorHandler: errorHandler)
    }
    
    
    //animation performed when user tap on a cell
    internal func performAnimation(fromIndex: Int) {
        //cell frame
        guard let cellAttr = collectionView.layoutAttributesForItem(at: IndexPath(row: fromIndex, section: 0)) else {return}
        let cellFrame = cellAttr.frame
        guard let cell = collectionView.cellForItem(at: IndexPath(row: fromIndex, section: 0)) else {return}
        
        let cellFrameToView = collectionView.convert(cellFrame, to: self.view)
        
        //txtf frame
        let screenFrame = UIScreen.main.bounds
        let shoppingCTabFrame = CGRect.init(
            x: screenFrame.width / 5 * 2 - screenFrame.width / 5,
            y: screenFrame.height - 50.0,
            width: screenFrame.width / 5,
            height: 50.0
        )
        
        //get txtf snapshot
        let cellSnapshot = cell.takeSnapshot()
        
        //create imgv
        let imgV = UIImageView(frame: cellFrameToView)
        imgV.image = cellSnapshot
        imgV.alpha = 0.4
        imgV.contentMode = .scaleAspectFit
        view.addSubview(imgV)
        
        //animate
        UIView.animate(withDuration: 0.35, delay: 0, options: [UIView.AnimationOptions.curveEaseInOut], animations: {
            imgV.frame = shoppingCTabFrame
            
        }) { (finish) in
            if finish {
                imgV.removeFromSuperview()
                
                //set badge according to order number
                if let allTabItems = self.tabBarController?.tabBar.items, allTabItems.count > 1 {
                    let shoppingC = allTabItems[1]
                    
                    //map products -> get array of int (userSelection) -> sum all
                    let bVal = Array(CurrentUsage.sharedInstance.productToOrder.values).map({$0.userSelectionNumber}).reduce(0, +)
                    shoppingC.badgeValue = "\(bVal)"
                }
            }
        }
    }
}

fileprivate extension UIView {
    func takeSnapshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
