//
//  LoginViewController.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 13/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit
import SwiftyGif

class LoginViewController: UIViewController {
    
    //UI Outlets
    @IBOutlet weak var usernameTxtF: BindingTextField! {
        didSet {
            usernameTxtF.bind(callback: {self.viewModel.username.value = $0})
        }
    }
    @IBOutlet weak var passwordTxtF: BindingTextField! {
        didSet {
            passwordTxtF.bind(callback: {self.viewModel.password.value = $0})
        }
    }
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var imageGifImage: UIImageView!
    fileprivate var GIFStartAnimating = false
    
    private var viewModel: LoginViewModel! {
        didSet {
            viewModel.username.bind = { [unowned self] in self.usernameTxtF.text = $0 }
            viewModel.password.bind = { [unowned self] in self.passwordTxtF.text = $0 }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9058823529, blue: 0.9137254902, alpha: 1)

        viewModel = LoginViewModel()
        usernameTxtF.delegate = self
        passwordTxtF.delegate = self
        
        //set gif to imageview
        let gif = UIImage(gifName: "flamingo.gif")
        imageGifImage.setGifImage(gif)
        imageGifImage.stopAnimatingGif()
    }
    
    
    @IBAction func loginButtonAction(_ sender: FLLoginButton) {
        //start animating
        sender.isEnabled = false
        imageGifImage.startAnimatingGif()
        
        viewModel.doAuth(successHandler: { (user) in
            //do something with user
            CurrentUsage.sharedInstance.currentUser = user
            
            //stop animating
            sender.isEnabled = true
            self.imageGifImage.stopAnimatingGif()
            
            //to go home VC
            self.performSegue(withIdentifier: "ToHomeSegue", sender: nil)
            
        }) { (authError) in
            
            //stop animating
            sender.isEnabled = true
            self.imageGifImage.stopAnimatingGif()
            
            switch authError {
                case AuthError.AccessForbidden :
                    break
                case AuthError.GenericError:
                    break
                case AuthError.MissingValues:
                    break
                case AuthError.ParameterError :
                    break
            }
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(LoginViewController.getHintsFromTextField(textField:)),
            object: textField)
        self.perform(
            #selector(LoginViewController.getHintsFromTextField(textField:)),
            with: textField,
            afterDelay: 2.0)
        
        if !GIFStartAnimating {
            GIFStartAnimating = true
            imageGifImage.startAnimatingGif()
        }
        
        return true
    }
    
    @objc func getHintsFromTextField(textField: UITextField) {
        if self.GIFStartAnimating {
            self.imageGifImage.stopAnimatingGif()
            self.GIFStartAnimating = false
        }
    }
}
