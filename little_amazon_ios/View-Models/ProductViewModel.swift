//
//  ProductViewModel.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 20/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class ProductViewModel {
    
    public enum ProductError {
        case Generic
        case Network
    }
    
    public typealias ProductRetriveSuccess = ()->()
    public typealias ProductRetriveError = (ProductError)->()
    
    private var allProducts: [Product] = []
    
    public func retriveData(successHandler: @escaping ProductRetriveSuccess, errorHandler: @escaping ProductRetriveError) {
        NetworkManager.get_shared().get_all_products(on_success: { (products) in
            self.allProducts.removeAll()
            self.allProducts = products
            successHandler()
        }, on_generic_error: {
            errorHandler(.Generic)
        }) {
            errorHandler(.Network)
        }
    }
    
    public func numberOfRows() -> Int {
        return allProducts.count
    }
    
    public func productForRow(index: Int) -> Product {
        return allProducts[index]
    }
}
