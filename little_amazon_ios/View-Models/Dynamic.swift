//
//  Dynamic.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 14/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//


import Foundation

class Dynamic<T> {
    
    var bind :(T) -> () = { _ in }
    
    var value :T? {
        didSet {
            bind(value!)
        }
    }
    
    init(_ v :T) {
        value = v
    }
    
}
