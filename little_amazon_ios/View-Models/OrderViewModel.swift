//
//  OrderViewModel.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 24/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class OrderViewModel {
    
    private var allOrders: [Order] = []
    
    public func getRowNumber() -> Int {
        return allOrders.count
    }
    
    public func getOrderAtIndex(index: Int) -> Order {
        return allOrders[index]
    }
    
    public func retriveOrders(success: @escaping ()-> (),
                              error: @escaping ()->()) {
        guard let user = CurrentUsage.sharedInstance.currentUser else {
            return
        }
        
        NetworkManager.get_shared().get_order_for_user(user: user, on_success: { (allOrders) in
            self.allOrders = allOrders
            success()
        }, on_error: error, on_network_error: error)
    }
    
    public func getAmount(for order: Order, price: NetworkManager.AmountType, success: @escaping (Int) -> (), error: @escaping ()->()) {
        NetworkManager.get_shared().getAmount(for: order, type: price, success: success, errorHandler: error)
    }
    
}
