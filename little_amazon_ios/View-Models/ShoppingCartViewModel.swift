//
//  ShoppingCartViewModel.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 20/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class ShoppingCartViewModel {
    
    //get number of products to return
    public func getTableRowNumber() -> Int {
        return CurrentUsage.sharedInstance.productToOrder.count
    }
    
    //get product at index for table cell
    public func getProductForIndex(index: Int) -> Product {
        return Array(CurrentUsage.sharedInstance.productToOrder.values)[index]
    }
   
    //retrive total for button
    public func retriveButtonTotalAmount() -> Int {
        var amount : Int = 0
        for p in Array(CurrentUsage.sharedInstance.productToOrder.values) {
            amount += (p.price * p.userSelectionNumber)
        }
        return amount
    }
}
