//
//  LoginViewModel.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 14/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

enum AuthError {
    case AccessForbidden
    case GenericError
    case ParameterError
    case MissingValues
}

class LoginViewModel {
    
    public typealias AuthSuccessHandler = (User) -> ()
    public typealias AuthErrorHandler = (AuthError) -> ()
    
    public var username: Dynamic<String> = Dynamic("")
    public var password: Dynamic<String> = Dynamic("")
    
    func doAuth(successHandler: @escaping AuthSuccessHandler, errorHandler: @escaping AuthErrorHandler) {
        if let u = username.value, let p = password.value {
            NetworkManager.get_shared().login(email: u, password: p, on_success: successHandler, on_generic_error: {errorHandler(.GenericError)}, on_access_forbidden: {errorHandler(.AccessForbidden)}, on_parameter_error: {errorHandler(.ParameterError)})
        } else {
            errorHandler(.MissingValues)
        }
        
    }
}
