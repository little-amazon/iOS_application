//
//  ProfileViewModel.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 20/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class ProfileViewModel {
    
    public func numberRowForTable() -> Int {
        return 2
    }
    
    //return a key-value
    public func getInfoCell(at index: Int) -> (String, String) {
        if let user = CurrentUsage.sharedInstance.currentUser {
            if index == 0 {
                return ("First name", user.name)
            }
            return ("Last name", user.surname)
        }
        return ("Unknown", "Unknown")
    }
}
