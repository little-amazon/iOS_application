//
//  Product.swift
//  little_amazon_ios
//
//  Created by Massimiliano Ceriani on 17/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class Product: NSObject {
    
    /*
     product_service_product:
     type: object
     required:
     - id
     - name
     - inserted_date
     - price
     properties:
     id:
     type: integer
     format: uuid
     example: 5349589045
     name:
     type: string
     example: Alexa
     inserted_date:
     type: string
     format: date-time
     example: 2016-08-29T09:12:33.001Z
     price:
     type: number
     example: 12.90
     */
    
    public var id: String!
    public var name: String!
    public var itemsNumber: Int!
    public var price: Int!
    public var urlImage: String?
    public var userSelectionNumber: Int = 0
    
    init(id: String, name: String, itemsNumber: Int, price: Int){
        self.id = id
        self.name = name
        self.itemsNumber = itemsNumber
        self.price = price
    }
    
    public func decrementProduct() {
        itemsNumber -= 1
        userSelectionNumber += 1
    }
    
    public func incrementProduct() {
        itemsNumber += 1
        userSelectionNumber -= 1
    }

}
