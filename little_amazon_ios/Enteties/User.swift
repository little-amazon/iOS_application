//
//  User.swift
//  little_amazon_ios
//
//  Created by Massimiliano Ceriani on 13/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class User: NSObject {
    
    /*
     
     
     {
     "_id": "8c7def52fdafa6e9e4e4f1ea4d001515",
     "_rev": "1-d6e3d2a5932cdbff513bee1427bc637f",
     "name": "MAX",
     "surname": "cer",
     "email": "email@email.com",
     "password_md5": "passowrd",
     "date": 1544723476900
     }
     */
    
    public var id: String!
    public var rev: String!
    public var name: String!
    public var surname: String!
    public var email: String!
    public var password_md5: String!
    public var date: Int!
    
    init(id: String, rev: String, name: String, surname: String, email: String, password: String, date: Int){
        self.id = id
        self.rev = rev
        self.name = name
        self.surname = surname
        self.email = email
        self.password_md5 = password
        self.date = date
        
    }

}
