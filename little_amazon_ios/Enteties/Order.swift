//
//  Order.swift
//  little_amazon_ios
//
//  Created by Massimiliano Ceriani on 17/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class Order: NSObject {
    
    /*
     order_item:
     type: object
     required:
     - idOrder
     - idUSer
     - date
     - product_list
     properties:
     idOrder:
     type: integer
     format: uuid
     example: 5349589045
     idUSer:
     type: integer
     format: uuid
     example: 32
     date:
     type: string
     format: date-time
     example: 2016-08-29T09:12:33.001Z
     
     product_list:
     type: array
     format: application/json
     items:
     type: object
     format: application/json
     example: "{
     'idProduct': 1 ,
     'quantity' : 78,
     'single_price' : 19.9
     }"
     */

    public var id: String!
    public var idUser: String!
    public var date: String!
    public var products: [Product]?
    
    init(id: String, idUser: String, date: String, products: [Product]?){
        self.id = id
        self.idUser = idUser
        self.date = date
        self.products = products
    }
}
