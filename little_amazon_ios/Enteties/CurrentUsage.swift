//
//  CurrentUsage.swift
//  little_amazon_ios
//
//  Created by Davide Ceresola on 19/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import Foundation

class CurrentUsage {
    public static let sharedInstance: CurrentUsage = CurrentUsage()
    
    public var currentUser: User?
    public var productToOrder: [String: Product] = [:] //id of product and product
    
    //when an order is done
    public func emptyProduct() {
        productToOrder.removeAll()
    }
    
    //when user's logging out
    public func closeSession() {
        currentUser = nil
        productToOrder.removeAll()
    }
}
