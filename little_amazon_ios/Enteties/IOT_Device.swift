//
//  IOT_Device.swift
//  little_amazon_ios
//
//  Created by Massimiliano Ceriani on 17/12/2018.
//  Copyright © 2018 flamingo. All rights reserved.
//

import UIKit

class IOT_Device: NSObject {
    
    /*
     iot_item:
     type: object
     required:
     - id
     - userID
     - productID
     properties:
     id:
     type: integer
     example: 5349589045
     userID:
     type: string
     example: 2222
     productID:
     type: string
     example: 3232
     quantity:
     type: integer
     example: 3232

    */

    public var id: Int!
    public var userID: Int!
    public var productID: Int!
    
    init(id: Int, userID: Int, productID: Int){
        self.id = id
        self.userID = userID
        self.productID = productID
    }
}
